from depthnet import get_depth_net, Gradient, Selection, Depth
from keras.callbacks import TensorBoard, ModelCheckpoint, ProgbarLogger
from keras.layers import Input, concatenate, Conv2D, MaxPooling2D, Conv2DTranspose, BatchNormalization, Lambda
from keras.utils import np_utils
from keras.models import model_from_yaml
from pprint import pprint
from sklearn.utils import shuffle
import multiprocessing as mp
from functools import partial
from skimage.measure import compare_ssim as ssim

import os
import json
import cv2
import gc
import sys
import numpy as np
import math
import random
import re

MAP_TYPES = 10

def gradients(imgs):
	gradients = np.gradient(np.average(imgs.astype(np.float32), axis = 3), axis = (1, 2))
	gradients = ((gradients[0] + gradients[1]))[:, 2:-2, 2:-2]
	return gradients

def rectilinear_to_equirectangular(v_pos, v_zero_angular, v_half, zoom = 1):
	v_angular = (v_pos - v_half) / v_half / 2 * np.pi
	v_angular[0] -= v_zero_angular[0]
	v_sines = np.sin(v_angular)
	v_cosines = np.sqrt(1 - v_sines*v_sines)
	sin_zero = np.sin(v_zero_angular[1])
	cos_zero = np.sqrt(1 - sin_zero*sin_zero)
	cos_c = sin_zero * v_sines[1] + cos_zero * v_cosines[1] * v_cosines[0]
	v_r = np.array((v_cosines[1] * v_sines[0], cos_zero * v_sines[1] + sin_zero * v_cosines[1] * v_cosines[0])) / cos_c
	return v_half + v_r * v_half / zoom

def equirectangular_to_rectilinear(v_pos, v_zero_angular, v_half, zoom = 1):
	v_relative = (v_pos - v_half) / v_half
	p = np.sqrt(np.dot(v_relative, v_relative))
	if p == 0.0:
		return v_half
	c = np.arctan(p)
	sin_zero = np.sin(v_zero_angular[1])
	cos_zero = np.sqrt(1 - sin_zero*sin_zero)
	sin_c = np.sin(c)
	cos_c = sin_c / p
	theta = v_zero_angular[0] + np.arctan2(v_relative[0] * sin_c, p * cos_zero * cos_c - v_relative[1] * sin_zero * sin_c)
	phi = np.arcsin(cos_c * sin_zero + (v_relative[1] * sin_c * cos_zero) / p)
	v_r = np.array((theta, phi)) / np.pi * 2
	return v_half + v_half * v_r
		
def get_transform_maps(v_orig_img_size, prefix):
	
	v_half_size = v_orig_img_size // 2
	maps = []
	if os.path.isfile(prefix + "_distortion_maps.npz"):
		try:
			print('Loading image transformation maps...')
			maps = np.load(prefix + "_distortion_maps.npz", "r")["maps"]
			return maps
		except IOError:
			maps = []
	
	print('Building image transformation maps...')
	rotations = [ # 2, 4 = x rot, 1, 3 = y rot, 0 = nothing, all rotate 30 degrees
		np.array((0., np.pi)),
		np.array((np.pi / 6, np.pi)),
		np.array((0., np.pi + np.pi / 6)),
		np.array((-np.pi / 6, np.pi)),
		np.array((0., np.pi - np.pi / 6))
	]
	for rot_id in range(MAP_TYPES // 2):
		map = np.zeros((v_orig_img_size[1], v_orig_img_size[0], 2),dtype=np.float32)
		map_mirror = np.zeros((v_orig_img_size[1], v_orig_img_size[0], 2),dtype=np.float32)
		for v_pos in np.ndindex(v_orig_img_size[0], v_orig_img_size[1]):
			v_calc = equirectangular_to_rectilinear(v_pos, rotations[rot_id], v_orig_img_size // 2)
			map[v_pos[1]][v_pos[0]] = v_calc
			v_calc[0] = v_half_size[0] - (v_calc[0] - v_half_size[0]) 
			map_mirror[v_pos[1]][v_pos[0]] = v_calc #mirror both images
			
		offset = np.array((v_orig_img_size[0], 0.), dtype=np.float32)
		map = np.concatenate((map, map + offset), axis = 1)
		maps.append(map)
		map_mirror = np.concatenate((map_mirror + offset, map_mirror), axis = 1)
		maps.append(map_mirror)
		
	np.savez(prefix + "_distortion_maps.npz", maps=maps)
	return maps

def prepare_frame(frame, map, v_orig_img_size, v_img_size, no_transform_maps):
	if no_transform_maps:
		return cv2.resize(frame, (v_img_size[0] * 2, v_img_size[1]))
	return cv2.resize(cv2.remap(cv2.resize(frame, (v_orig_img_size[0] * 2, v_orig_img_size[1])), map, None, cv2.INTER_LINEAR), (v_img_size[0] * 2, v_img_size[1]))
	
def get_frames(video, take=None):
	frames = []
	n = 1
	success = True
	while success and (take >= n or take is None):
		success, frame = video.read()
		if success:
			frames.append(frame)
		n += 1
	return success, frames
	
def prepare_video(video_path, prefix, directory_path, frame_freq, v_orig_img_size, v_img_size, clip_at, maps, structural_similarity_treshold, no_transform_maps, no_augmentation_maps):
	
	map_types = MAP_TYPES
	if no_transform_maps or no_augmentation_maps:
		map_types = 1
	
	try:
		video = cv2.VideoCapture(video_path)
		success, frame = video.read()
		written = 0
		writers = []
		pos = video_path.rfind('\\') + 1
		clip_at_pos = clip_at
		if success:
			try:
				pending_frames = [[] for n in range(map_types)]
				pending_frames[0].append(prepare_frame(frame, maps[0], v_orig_img_size, v_img_size, no_transform_maps))
				last_written = [np.zeros_like(pending_frames[0][0]) for n in range(map_types)]
				last_written[0] = pending_frames[0][0]
				print('Preparing videos for {}...'.format(video_path))
				print('This will take a while, but only needs to be done once per resolution or frame skip change.')
				for i in range(map_types):
					processed_path = directory_path + "\\processed\\" + prefix +  "type" + str(i) + "_" + video_path[pos:]
					writers.append(cv2.VideoWriter(processed_path, cv2.VideoWriter_fourcc(*'X264'), 30.0, (v_img_size[0] * 2, v_img_size[1])))
				n = 0
				while success:
					map_type = int((n / frame_freq) % map_types)
					if n > clip_at_pos:
						for idx in range(len(writers)):
							writers[idx].release()
							processed_path = directory_path + "\\processed\\" + prefix +  "type" + str(i) + "_part" + str(1 + n // clip_at) + "_" + video_path[pos:]
							writers[idx] = cv2.VideoWriter(processed_path, cv2.VideoWriter_fourcc(*'X264'), 30.0, (v_img_size[0] * 2, v_img_size[1]))
						clip_at_pos += clip_at
					if n % 10000 == 0 and n:
						print('Frame #{} processed.'.format(n))
					if n % frame_freq == 0:
						pending_frames[map_type].append(prepare_frame(frame, maps[map_type], v_orig_img_size, v_img_size, no_transform_maps))
						
					success, frame = video.read()
					n += 1
					
					if not success or len(pending_frames[map_type]) > 20:
						#np_pending_frames = np.array(pending_frames)
						#last_ones = np.roll(np_pending_frames, 1)
						#last_ones[0] = np.zeros_like(last_ones[0])
						#diffs = MultiScaleSSIM(np_pending_frames, last_ones, max_val=255)
						for idx in range(len(pending_frames[map_type])):
							diff = ssim(cv2.cvtColor(last_written[map_type], cv2.COLOR_BGR2GRAY), cv2.cvtColor(pending_frames[map_type][idx], cv2.COLOR_BGR2GRAY))
							if diff < structural_similarity_treshold:
								last_written[map_type] = pending_frames[map_type][idx]
								writers[map_type].write(last_written[map_type])
								written += 1
						pending_frames[map_type] = []
				return written
			except Exception as e:
				for writer in writers:
					writer.release()
				if os.path.isfile(processed_path):
					os.remove(processed_path)
				print(e.message, e.args)
			finally:
				for writer in writers:
					writer.release()
	finally:
		video.release()
	
def prepare_videos(directory_path, prefix, n_threads, frame_freq, v_orig_img_size, v_img_size, maps, structural_similarity_treshold, clip_at = math.inf, no_transform_maps = False, no_augmentation_maps = False):
	initial = get_videos(directory_path)
	processed = get_videos(directory_path + "\\processed", prefix)
	todo = []
	for video_path in initial:
		pos = video_path.rfind('\\') + 1
		processed_path = directory_path + "\\processed\\" + prefix + "type0_" + video_path[pos:]
		if not processed_path in processed:
			todo.append(video_path)
	with mp.Pool(processes = n_threads) as pool:
		count = pool.map(partial(prepare_video, prefix = prefix, directory_path = directory_path, frame_freq = frame_freq, clip_at = clip_at, v_orig_img_size = v_orig_img_size, v_img_size = v_img_size, maps = maps, structural_similarity_treshold = structural_similarity_treshold, no_transform_maps = no_transform_maps, no_augmentation_maps = no_augmentation_maps), todo)
		total_count = np.sum(np.array(count))
		if total_count > 0:
			print("{} frames prepared in total.".format(total_count))
	cv2.destroyAllWindows()
	gc.collect()

def get_videos(data_path, prefix = ""):
	return [os.path.join(data_path, file_name)
			for file_name in os.listdir(data_path)
			if (os.path.isfile(os.path.join(data_path, file_name))
				and (".mkv" in file_name  or ".mp4" in file_name or ".m4v" in file_name) and file_name.startswith(prefix))]
				
def generate_data(videos, buffer_length, batch_size, v_img_size, stream_count = 1):
	video_streams = []
	try:
		if len(videos) == 0:
			return
		regex_rotated_x = re.compile("type[2367]_")
		stream_count = np.minimum(stream_count, len(videos))
		video_ids = random.sample(range(len(videos)), stream_count)
		video_paths = [videos[id] for id in video_ids]
		video_streams = [cv2.VideoCapture(video_path) for video_path in video_paths]
		collected_frames = [[] for n in range(stream_count)]
		while True:
			for idx, stream in enumerate(video_streams):
				if stream is None:
					while True:
						new_video_id = np.random.randint(len(videos))
						if not new_video_id in video_ids:
							video_ids[idx] = new_video_id
							break
					video_paths[idx] = videos[video_ids[idx]]
					stream = cv2.VideoCapture(video_paths[idx])
					video_streams[idx] = stream
					
				success, frames = get_frames(stream, buffer_length)
				collected_frames[idx] = frames
				if not success:
					stream.release()
					video_streams[idx] = None
			
			while any(frames != [] for frames in collected_frames):
				batch_from = 0
				while True:
					batch_from = np.random.randint(stream_count)
					if(collected_frames[batch_from] != []):
						break
				frames_batch = np.flip(np.vectorize(lambda i: i / 255.0)(np.array(collected_frames[batch_from][:batch_size])), axis=3) #BGR 0-255 to RGB 0-1
				collected_frames[batch_from] = collected_frames[batch_from][batch_size:]
				if(frames_batch.shape[0] == batch_size):
					shift_scale = 1.0
					if regex_rotated_x.match(video_paths[batch_from]):
						shift_scale = np.cos(np.pi / 6)
					if np.random.randint(2) == 0:
						frames_batch = np.flip(frames_batch, axis=2) #mirror
					left_input_batch = frames_batch[..., :v_img_size[0], :]
					right_output_batch = frames_batch[..., v_img_size[0]:, :]
					yield [left_input_batch, np.full((batch_size, 1, 1), shift_scale)], [right_output_batch, np.zeros(shape=(right_output_batch.shape[0], v_img_size[1] - 4, v_img_size[0] - 4))]

	finally:
		for video in video_streams:
			if video:
				video.release()
		cv2.destroyAllWindows()
		gc.collect()
		
def get_k_fold(videos, id_low, id_high, folds):
	training_videos = []
	validation_videos = []
	for i, video in enumerate(videos):
		if i % folds >= id_low and i % folds <= id_high:
			validation_videos.append(video)
		else:
			training_videos.append(video)
	return training_videos, validation_videos
		
def main():

	v_orig_img_size = np.array([512, 576])
	v_img_size = v_orig_img_size // 2
	n_threads = 3
	frame_freq = 1
	batch_size = 5
	buffer_batches = 50
	len_train_epoch = 5000
	len_valid_epoch = 1000
	len_eval_epoch = 500
	n_epochs = 20
	max_frames_per_video = 18000
	structural_similarity_treshold = 0.85
	video_stream_count = 6
	models_folder = 'models'
	model_name = 'distort3d_full_1x1_norot'
	load_epoch = 149
	load_name = 'distort3d_full_1x1_best_run_epoch_' + str(load_epoch)
	eval_only = False
	no_transform_maps = False
	no_augmentation_maps = True
	
	video_prefix = "{}w{}h{}f30d_".format(v_img_size[0] * 2, v_img_size[1], MAP_TYPES * frame_freq)
	maps = get_transform_maps(v_orig_img_size, video_prefix)
	if not eval_only:
		prepare_videos("train", video_prefix, n_threads, frame_freq, v_orig_img_size, v_img_size, maps, structural_similarity_treshold, max_frames_per_video, no_transform_maps = no_transform_maps, no_augmentation_maps = no_augmentation_maps)
	prepare_videos("evaluation", video_prefix, n_threads, 5, v_orig_img_size, v_img_size, maps, structural_similarity_treshold, max_frames_per_video, no_transform_maps = no_transform_maps, no_augmentation_maps = no_augmentation_maps)

	videos = get_videos("train\\processed", video_prefix)
	evaluation_videos = get_videos("evaluation\\processed", video_prefix)
	evaluation_data = generate_data(evaluation_videos, batch_size * buffer_batches, batch_size, v_img_size, stream_count = 3)
	
	model_path = os.path.join(models_folder, model_name)
		
	if eval_only:
		loaded_model = None
		print('Loading model {}...'.format(model_path))
		with open(model_path + ".yaml", "r") as yaml_file:
			loaded_model, _ = get_depth_net(img_rows=v_img_size[1], img_cols=v_img_size[0],  disparity_levels = range(-16,16,1))
			#loaded_model = model_from_yaml(yaml_file.read(), custom_objects = { 'Gradient' : Gradient, 'Depth' : Depth, 'Selection' : Selection })
			#print("Loaded model from disk")
		
		print('Begin evaluating model {}, {} epochs...'.format(model_name, n_epochs))
		for epoch in range(load_epoch, n_epochs):
			print ("Epoch {}".format(epoch + 1))
			
			model_path = os.path.join(models_folder, model_name + '_epoch_{}'.format(epoch + 1) + '.h5')
			loaded_model.load_weights(model_path)

			print("Evaluation: " + str(loaded_model.evaluate_generator(evaluation_data, steps=len_eval_epoch // batch_size)))
		
	else:
		print('Found {} training samples.'.format(len(videos)))
		print('Building model...')
		loaded_model, _ = get_depth_net(img_rows=v_img_size[1], img_cols=v_img_size[0])
		

		load_path = os.path.join(models_folder, load_name) + ".h5"
		if os.path.isfile(load_path):
			print('Loading model {}...'.format(load_path))
			loaded_model.load_weights(load_path)
		else:
			load_epoch = 0
		
		print('Saving model to {}...'.format(model_path))
		model_yaml = loaded_model.to_yaml()
		with open(model_path + ".yaml", "w") as yaml_file:
			yaml_file.write(model_yaml)
		
		print('Begin training model, {} epochs...'.format(n_epochs))
		for epoch in range(load_epoch, n_epochs):
			print ("Training epoch {}".format(epoch + 1))
			
			training_videos, validation_videos = get_k_fold(videos, epoch, epoch + 1, n_epochs)
			training_data = generate_data(training_videos, batch_size * buffer_batches, batch_size, v_img_size, video_stream_count)
			validation_data = generate_data(validation_videos, batch_size * buffer_batches, batch_size, v_img_size, video_stream_count)
			
			model_path = os.path.join(models_folder, model_name + '_epoch_{}'.format(epoch + 1))
			
			loaded_model.fit_generator(generator = training_data,
						steps_per_epoch = len_train_epoch // batch_size,
						epochs = 1,
						validation_data = validation_data,
						validation_steps = len_valid_epoch // batch_size,
						verbose = 2,
						pickle_safe = False,
						callbacks=[TensorBoard(log_dir='/tmp/temp3d1'),
									ModelCheckpoint(model_path + '.h5',
												monitor = 'loss',
												verbose = 0,
												save_best_only = False,
												save_weights_only = True,
												mode = 'auto', 
												period = 1)])

			print("Evaluation: " + str(loaded_model.evaluate_generator(evaluation_data, steps=len_eval_epoch // batch_size)))

if __name__ == '__main__':
	main()
