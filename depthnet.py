from __future__ import print_function

from keras import backend as K
from keras.layers import Dropout
from keras.layers import Input, concatenate, Conv2D, MaxPooling2D, Conv2DTranspose, BatchNormalization, Lambda, UpSampling2D, UpSampling3D, Reshape, ZeroPadding2D
from keras.layers.convolutional import SeparableConv2D
from keras.models import Model
from keras.optimizers import Adam
from keras.engine.topology import Layer

import os
os.environ["TF_CPP_MIN_LOG_LEVEL"]="3"

K.set_image_data_format('channels_last')  # TF dimension ordering in this code


class Selection(Layer):
	def __init__(self, disparity_levels=None, **kwargs):
		# if none, initialize the disparity levels as described in deep3d
		if disparity_levels is None:
			disparity_levels = range(-16, 16, 1)

		super(Selection, self).__init__(**kwargs)

		self.disparity_levels = disparity_levels

	def build(self, input_shape):
		# Used purely for shape validation.
		if not isinstance(input_shape, list) or len(input_shape) != 2:
			raise ValueError('A `Selection` layer should be called on a list of 2 inputs.')

	def call(self, inputs):

		# first we extract the left image from the original input
		image = inputs[0]
		# then the calculated disparity map that is the ouput of the Unet
		disparity_map = inputs[1]
		# initialize the stack of shifted left images
		shifted_images = []
		disparity_mul = []

		# loop over the different disparity levels and shift the left image accordingly, add it to the list
		for shift in self.disparity_levels:
			if shift > 0:
				shifted_images += [K.concatenate([image[..., shift:, :], K.zeros_like(image[..., :shift, :])], axis=2)]
			elif shift < 0:
				shifted_images += [K.concatenate([K.zeros_like(image[..., shift:, :]), image[..., :shift, :]], axis=2)]
			else:
				shifted_images += [image]


		# create a tensor of shape (None, im_rows, im_cols, disparity_levels)

		shifted_images_stack = K.stack(shifted_images)
		shifted_images_stack = K.permute_dimensions(shifted_images_stack, (1, 2, 3, 0, 4))
		
		
		# take the dot product with the disparity map along the disparity axis
		# and output the resulting right image of size (None, im_rows, im_cols)
		new_image = []
		for ch in range(3):
			new_image += [K.sum(shifted_images_stack[..., ch] * disparity_map, axis=3)]

		new_image = K.stack(new_image)
		new_image = K.permute_dimensions(new_image, (1, 2, 3, 0))

		return new_image

	def compute_output_shape(self, input_shape):
		return input_shape[0]


class Gradient(Layer):
	def __init__(self, **kwargs):
		# if none, initialize the disparity levels as described in deep3d
		super(Gradient, self).__init__(**kwargs)

	def build(self, input_shape):
		# Used purely for shape validation.
		pass

	def call(self, inputs):
		dinputs_dx_0 = inputs - K.concatenate([K.zeros_like(inputs[..., :1, :]), inputs[..., :-1, :]], axis=1)
		dinputs_dx_1 = inputs - K.concatenate([inputs[..., 1:, :], K.zeros_like(inputs[..., :1, :])], axis=1)

		dinputs_dy_0 = inputs - K.concatenate([K.zeros_like(inputs[..., :1]), inputs[..., :-1]], axis=2)
		dinputs_dy_1 = inputs - K.concatenate([inputs[..., 1:], K.zeros_like(inputs[..., :1])], axis=2)

		abs_gradient_sum = 0.25 * K.sqrt(
			K.square(dinputs_dx_0) + K.square(dinputs_dx_1) + K.square(dinputs_dy_0) + K.square(dinputs_dy_1))

		return abs_gradient_sum[..., 2:-2, 2:-2]

	def compute_output_shape(self, input_shape):
		return (input_shape[0], input_shape[1] - 4, input_shape[2] - 4)
		
		
class HorizontalGradient(Layer):
	def __init__(self, **kwargs):
		# if none, initialize the disparity levels as described in deep3d
		super(Gradient, self).__init__(**kwargs)

	def build(self, input_shape):
		# Used purely for shape validation.
		pass

	def call(self, inputs):
		dinputs_dx_0 = inputs - K.concatenate([K.zeros_like(inputs[..., :1, :]), inputs[..., :-1, :]], axis=1)
		dinputs_dx_1 = inputs - K.concatenate([inputs[..., 1:, :], K.zeros_like(inputs[..., :1, :])], axis=1)

		abs_gradient_sum = 0.5 * K.sqrt(K.square(dinputs_dx_0) + K.square(dinputs_dx_1))

		return abs_gradient_sum[..., 2:-2, 2:-2]

	def compute_output_shape(self, input_shape):
		return (input_shape[0], input_shape[1] - 4, input_shape[2] - 4)


class Depth(Layer):
	def __init__(self, disparity_levels=None, **kwargs):
		# if none, initialize the disparity levels as described in deep3d
		if disparity_levels is None:
			disparity_levels = range(-16, 16, 1)

		super(Depth, self).__init__(**kwargs)

		self.disparity_levels = disparity_levels

	def build(self, input_shape):
		# Used purely for shape validation.
		pass

	def call(self, inputs):
		disparity = inputs[0]
		depth = []
		for n, disp in enumerate(self.disparity_levels):
			depth += [disparity[..., n] * disp]

		depth = K.concatenate(depth, axis=0)
		return K.sum(depth, axis=0, keepdims=True)

	def compute_output_shape(self, input_shape):
		return input_shape[:-1]


K.set_image_data_format('channels_last')  # TF dimension ordering in this code

	
def upscaler2(input, ratio, feature_count, interpolate_kernel, interpolate_dilation_rate = 1):
	conv = Conv2D(feature_count, (1, 1), padding='same')(input)
	conv = BatchNormalization()(conv)
	up = UpSampling2D(size=ratio)(conv)
	interpolated = SeparableConv2D(feature_count, interpolate_kernel, dilation_rate=interpolate_dilation_rate, padding='same')(up)
	interpolated = BatchNormalization()(interpolated)
	return interpolated
	
def upscaler(input, ratio, feature_count, interpolate_kernel, interpolate_dilation_rate = 1):
	conv = Conv2DTranspose(feature_count, ratio, strides = ratio, padding='same')(input)
	conv = BatchNormalization()(conv)
	interpolated = SeparableConv2D(feature_count, interpolate_kernel, dilation_rate = interpolate_dilation_rate, padding='same')(conv)
	interpolated = BatchNormalization()(interpolated)
	return interpolated
	
def separableConvBlock(input, feature_count):
	conv = SeparableConv2D(feature_count, (3, 3), activation='relu', padding='same')(input)
	conv = BatchNormalization()(conv)
	conv = SeparableConv2D(feature_count, (3, 3), activation='relu', padding='same')(conv)
	conv = BatchNormalization()(conv)
	return conv
	
def upconv2xBlock(input_small, input, feature_count_in, feature_count_out):
	up = concatenate([Conv2DTranspose(feature_count_in, (2, 2), strides=(2, 2), padding='same')(input_small), input], axis=3)
	conv = Conv2D(feature_count_in, (3, 3), activation='relu', padding='same')(up)
	conv = BatchNormalization()(conv)
	conv = SeparableConv2D(feature_count_out, (3, 3), activation='relu', padding='same')(conv)
	conv = BatchNormalization()(conv)
	conv = Dropout(rate=0.4)(conv)
	return conv

	
def get_depth_net(img_rows, img_cols, disparity_levels = range(-16, 16, 1)):
	input_image = Input(shape=(img_rows, img_cols, 3))
	
	horizontal_shift_scale = Input(shape=(1, 1))
	
	layer_multiple = 16
			
	input_im_gray = Lambda(lambda x: K.mean(x, axis=3))(input_image)
	input_im_gray_norm = Lambda(lambda x: x / K.max(x))(input_im_gray)
	image_gradient_gray = Gradient()(input_im_gray_norm)

	gradient = ZeroPadding2D(padding=2)(Reshape((img_rows - 4, img_cols - 4, 1))(image_gradient_gray))
	
	input_grad = concatenate([input_image, gradient], axis=3)
	
	conv1 = separableConvBlock(input_grad, layer_multiple * 2)
	pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
	
	conv2 = separableConvBlock(pool1, layer_multiple * 2)
	pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
	
	conv3 = separableConvBlock(pool2, layer_multiple * 4)
	pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
	
	conv4 = separableConvBlock(pool3, layer_multiple * 8)
	pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)
	
	conv5 = separableConvBlock(pool4, layer_multiple * 16)

	conv6 = upconv2xBlock(conv5, conv4, layer_multiple * 8, layer_multiple * 8)
	
	conv7 = upconv2xBlock(conv6, conv3, layer_multiple * 4, layer_multiple * 4)
	
	conv8 = upconv2xBlock(conv7, conv2, layer_multiple * 2, layer_multiple * 2)
	
	disparity_level_1 = upconv2xBlock(conv8, conv1, layer_multiple * 2, layer_multiple * 2)

	disparity_level_2 = upscaler2(pool2, (4, 4), layer_multiple, interpolate_kernel=(5, 5))

	disparity_level_3 = upscaler2(pool3, (8, 8), layer_multiple, interpolate_kernel=(7, 7), interpolate_dilation_rate=2)
	
	disparity_level_4 = upscaler2(pool4, (16, 16), layer_multiple, interpolate_kernel=(9, 9), interpolate_dilation_rate=3)
	
	horizontal_shift_scale_resized = UpSampling2D(size=(img_rows, img_cols), input_shape=(1,))(Reshape((1, 1, 1), input_shape=(1,))(horizontal_shift_scale))
	
	pre_disparity = concatenate([disparity_level_1, disparity_level_2, disparity_level_3, disparity_level_4, horizontal_shift_scale_resized], axis=3)	
	
	# use a softmax activation on the conv layer output to get a probabilistic disparity map
	final_disparity = Conv2D(32, (1, 1), activation='softmax', padding='same')(pre_disparity)

	output = Selection(disparity_levels=disparity_levels)([input_image, final_disparity])
	
	# gradient regularization
	depth = Depth(disparity_levels=disparity_levels)([final_disparity])
	depth_gradient = Gradient()(depth)

	
	weighted_gradient = Lambda(lambda x: x[0] * (1 - x[1]))([depth_gradient, image_gradient_gray])
	
	output_im_gray = Lambda(lambda x: K.mean(x, axis=3))(output)
	output_im_gray_norm = Lambda(lambda x: x / K.max(x))(output_im_gray)
	output_gradient = Gradient()(output_im_gray_norm)
	output_inv_gradient = Lambda(lambda x: 1 - x)(output_gradient)
	
	model = Model(inputs=[input_image, horizontal_shift_scale], outputs=[output, weighted_gradient])

	short_disparity_levels = range(-8, 8, 1)
	disparity_level_1 = Depth(disparity_levels=short_disparity_levels)([disparity_level_1])
	disparity_level_2 = Depth(disparity_levels=short_disparity_levels)([disparity_level_2])
	disparity_level_3 = Depth(disparity_levels=short_disparity_levels)([disparity_level_3])
	disparity_level_4 = Depth(disparity_levels=short_disparity_levels)([disparity_level_4])

	disp_map_model = Model(inputs=[input_image, horizontal_shift_scale], outputs=[depth, disparity_level_1, disparity_level_2, disparity_level_3, disparity_level_4])

	# we use L1 type loss as it has been shown to work better for that type of problem in the deep3d paper
	# (https://arxiv.org/abs/1604.03650)
	model.compile(optimizer=Adam(lr=1e-4), loss='mean_absolute_error', loss_weights=[1., 0.001])
	model.summary()

	return model, disp_map_model